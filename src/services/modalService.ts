import Vue, { VueConstructor } from 'vue';

export default {
  open(modalComponent: VueConstructor) {
    return new Promise((resolve, reject) => {
      const ModalWrapper = Vue.component('modal', {
        data() {
          return {
            show: true,
          };
        },
        components: {
          ModalComponent: modalComponent,
        },
        methods: {
          decline() {
            this.show = false;
            reject();
          },
          accept() {
            this.show = false;
            resolve();
          },
        },
        template: '<modal-component v-if="show" v-on:decline="decline" v-on:accept="accept" />',
      });

      const modalInstance = new ModalWrapper();

      modalInstance.$mount();

      document.getElementById('app').appendChild(modalInstance.$el);
    });
  },
};
